if [[ -f lambda.zip ]]; then rm lambda.zip; else echo "create deployment package:"; fi
mkdir -p tmp
cp lambda-src-code/* tmp
cd tmp
if [[ -f requirements.txt ]]; then pip install --upgrade -t . -r requirements.txt; else echo "no dependency"; fi
zip -u -r9 ../lambda.zip .
cd ..
rm -rf tmp