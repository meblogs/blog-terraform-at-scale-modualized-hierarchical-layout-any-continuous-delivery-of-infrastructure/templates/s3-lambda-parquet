import pandas as pd

def lambda_handler(event, context):
    print(event)
    bucket_name = event["Records"][0]["s3"]["bucket"]["name"]
    key = event["Records"][0]["s3"]["object"]["key"]
    filename = key.split("/")[-1].split(".")[0]
    data = pd.read_csv(f"s3://{bucket_name}/{key}")
    print(data.head())
    data.to_parquet(
        f"s3://{bucket_name}/processed/{filename}.parquet",
        partition_cols = None
    )


if __name__ == "__main__":
    event = {'Records': 
        [{
            'eventVersion': '2.1', 
            'eventSource': 'aws:s3', 
            'awsRegion': 'eu-central-1', 
            'eventTime': '2020-09-23T13:39:26.047Z', 
            'eventName': 'ObjectCreated:Put', 
            'userIdentity': {'principalId': 'AVZCS7MRLV6UM'}, 
            'requestParameters': {'sourceIPAddress': '82.82.37.94'}, 
            'responseElements': {
                'x-amz-request-id': '6EB8D19492815F55', 
                'x-amz-id-2': 'UHW+qsC/MMj3XBZemHuFHhRnZNA6wVzL/CMNxIBbhuBys8rBYjOgnY9KjBU4BchMrjSxQoIA+a4u01SfHDzSc1Z1Mnq4bOoy'
            }, 
            's3': {
                's3SchemaVersion': '1.0', 
                'configurationId': 'tf-s3-lambda-20200923102325900800000002', 
                'bucket': {'name': 'db-analytics-data-hub-1-sdf', 
                'ownerIdentity': {'principalId': 'AVZCS7MRLV6UM'}, 
                'arn': 'arn:aws:s3:::db-analytics-data-hub-1-sdf'}, 
                'object': {
                    'key': 'raw/test_data.csv', 
                    'size': 39967, 
                    'eTag': 'e4b33c4ea3c03f333287230474e53378', 
                    'versionId': '1hoNMUPaGLe9GzduYW4n6cmD7qfyqlkJ', 
                    'sequencer': '005F6B50104929FE8A'
                    }
                }
            }]}


    lambda_handler(event, {})
