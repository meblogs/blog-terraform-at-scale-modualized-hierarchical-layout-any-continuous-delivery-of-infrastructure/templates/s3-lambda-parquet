# s3 bucket
module "datalake_s3_bucket" {
    source = "github.com/terraform-aws-modules/terraform-aws-s3-bucket"
    bucket = "db-analytics-data-hub-1-sdf"
    acl = "public-read-write"
    versioning = {
        enabled = false
    }
}

# processing lambda

resource "aws_s3_bucket_object" "object" {
  bucket = module.datalake_s3_bucket.this_s3_bucket_id
  key    = "/lambda_deployment_zip/lambda.zip"
  source = "lambda.zip"
  etag    = "${filemd5("${path.module}/lambda.zip")}"
  depends_on = [module.datalake_s3_bucket]
}

module "processing_lambda" {
  source = "git::https://gitlab.com/meblogs/blog-terraform-at-scale-modualized-hierarchical-layout-any-continuous-delivery-of-infrastructure/modules/aws-module-lambda.git?ref=master"
  filename = "${path.module}/lambda.zip"
  deploy_mode = "s3"
  s3_bucket =  module.datalake_s3_bucket.this_s3_bucket_id
  s3_key = "lambda_deployment_zip/lambda.zip"
  function_name = "converting_to_parquet"
  handler       = "converting_to_parquet.lambda_handler"
  timeout       = 900
  depends_on = [module.datalake_s3_bucket, aws_s3_bucket_object.object]
}



# trigger between s3 bucket and lambda
resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = module.processing_lambda.arn
  principal     = "s3.amazonaws.com"
  source_arn    = module.datalake_s3_bucket.this_s3_bucket_arn
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = module.datalake_s3_bucket.this_s3_bucket_id

  lambda_function {
    lambda_function_arn = module.processing_lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "raw/"
    filter_suffix       = ".csv"
  }

  depends_on = [aws_lambda_permission.allow_bucket, module.datalake_s3_bucket]
}