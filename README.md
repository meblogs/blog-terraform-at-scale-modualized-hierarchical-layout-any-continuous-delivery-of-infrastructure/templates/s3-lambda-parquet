# s3-lambda-parquet

S3 + Lambda (upload of csv trigger lambda to convert .csv into parquet)

## Architecture

![architecture](architecture.png)


## Reference

* https://docs.aws.amazon.com/lambda/latest/dg/with-s3-example.html
